import { IonButton, IonButtons, IonCard, IonContent, IonHeader, IonIcon, IonItem, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useHistory, useParams } from 'react-router';
import './Page.css';
import { IonGrid, IonRow, IonCol} from '@ionic/react';
import { add, close, pencil, search } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { deleteCustomer, saveCustomer, searchCustomers } from '../components/CustomerApi';
import { Customer } from '../components/Customer';

const CustomerList: React.FC = () => {

  const { name } = useParams<{ name: string; }>();
  const [clientes, setClientes] = useState<Customer[]>([]);
  const history = useHistory();

  useEffect( () => {
      search();
  },[history.location.pathname]);
  const search = async() =>{
    let result = await searchCustomers(); 
    setClientes(result);
  };
  const remove = async (id:string) =>{
      await deleteCustomer(id);
      search();
  };
  // const pruebaLocalStorage = () =>{
  //   const ejemplos = {
  //       id:"2",
  //       firstName: "Mario",
  //       LastName: "Bros",
  //       email: "mario@bros.com",
  //       phone: 2335671231,
  //       adress: "Av. 100pre viva #3"
  //     }
  //   //   ,{
  //   //     id:"3",
  //   //     firstName: "Zelda",
  //   //     LastName: "Cuatro",
  //   //     email: "zelda@boftw.com",
  //   //     phone: 5561231234,
  //   //     adress: "Av. Inferno #123"
  //   //   }
  //     saveCustomer(ejemplos);
  // };
  const addCustomer= () =>{
    history.push('/page/customer/new');
  };
  const editCustomer= (id:string) =>{
    history.push('/page/customer/'+id);
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{name}</IonTitle>
          </IonToolbar>
        </IonHeader>
        {/* <ExploreContainer name={name} /> */}


    <IonContent>
        <IonCard>
            <IonTitle>Customers</IonTitle>
            <IonItem>
            <IonButton onClick={addCustomer}  color="primary" fill='solid' size='default' slot='end'>
                <IonIcon  icon={add}></IonIcon>
                Add Customer</IonButton>
            </IonItem>
        <IonGrid class='table'>
      <IonRow>
        <IonCol> Nombre </IonCol>
        <IonCol> Email </IonCol>
        <IonCol> Telefono </IonCol>
        <IonCol> Direccion </IonCol>
        <IonCol> Acciones </IonCol>
      </IonRow>

      {clientes.map( (cliente: Customer) => 
          <IonRow>
          <IonCol> { cliente.firstname} { cliente.lastname}</IonCol>
          <IonCol> { cliente.email}</IonCol>
          <IonCol> { cliente.phone} </IonCol>
          <IonCol> { cliente.adress} </IonCol>
          <IonCol> 
          <IonButton shape='round' fill='clear' onClick={()=>editCustomer(String(cliente.id))} >
                  <IonIcon icon={pencil} slot='icon-only' color="primary" />
          </IonButton>
          <IonButton shape='round' fill='clear' onClick={() => remove(String(cliente.id))}>
                  <IonIcon icon={close} slot='icon-only' color="danger" />
          </IonButton>
          </IonCol>
        </IonRow>
      )}
      

      

    </IonGrid>
    </IonCard>
    {/* <IonButton onClick={pruebaLocalStorage} shape='round' fill='clear'>
                  Prueba local storage
          </IonButton> */}
    </IonContent>



      </IonContent>
    </IonPage>
  );
};

export default CustomerList;
