import { IonButton, IonButtons, IonCard, IonContent, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { useHistory, useParams, useRouteMatch } from 'react-router';
import './Page.css';
import { IonGrid, IonRow, IonCol } from '@ionic/react';
import { add, checkmark, close, pencil, search } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { deleteCustomer, saveCustomer, searchCustomerbyId, searchCustomers } from '../components/CustomerApi';
import { Customer } from '../components/Customer';

const CustomerEdit: React.FC = () => {

  const { name} = useParams<{ name: string }>();
  const [customer, setCustomer] = useState<Customer>({});
  const history = useHistory();
  const routeMatch:any = useRouteMatch("/page/customer/:id");
  const id =routeMatch?.params?.id;
  const save = async() => {
    await saveCustomer(customer);
    alert('Customer saved')
    history.push('/page/Customer')
  };
  // const [clientes, setClientes] = useState<any>([]);
  useEffect( () => {
      search();
  },[history.location.pathname]);

  const search = async () =>{
    if(id ==='new'){
      setCustomer({});
    }else{
      let result = await searchCustomerbyId(id);
      setCustomer(result);
    }
  };
  // const remove = (id:string) =>{
  //     deleteCustomer(id);
  //     search();
  // };
  // const pruebaLocalStorage = () =>{
  //   const ejemplos = {
  //       id:"2",
  //       firstName: "Mario",
  //       LastName: "Bros",
  //       email: "mario@bros.com",
  //       phone: 2335671231,
  //       adress: "Av. 100pre viva #3"
  //     }
  //     saveCustomer(ejemplos);
  // };


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{name}</IonTitle>
          </IonToolbar>
        </IonHeader>
        {/* <ExploreContainer name={name} /> */}


        <IonContent>
          <IonCard>
            <IonTitle>{id == 'new' ? 'Add Customer': 'Edit Customer'}</IonTitle>
            <IonItem>
              <IonButton onClick={save} color="success" fill='solid' size='default' slot='end'>
                <IonIcon icon={checkmark}></IonIcon>
                Save -{id} </IonButton>
            </IonItem>
            <IonGrid class='table'>
              <IonRow>
                <IonCol>
                  <IonItem>
                    <IonLabel position="stacked">Nombre</IonLabel>
                    <IonInput onIonChange={e => customer.firstname=String(e.detail.value)}  >
                      {customer.firstname} </IonInput>
                  </IonItem>
                </IonCol>
                <IonCol>
                <IonItem>
                    <IonLabel position="stacked">Apellido</IonLabel>
                    <IonInput onIonChange={e => customer.lastname=String(e.detail.value)} >
                      {customer.lastname} </IonInput>
                  </IonItem>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <IonItem>
                    <IonLabel position="stacked">Email</IonLabel>
                    <IonInput onIonChange={e => customer.email=String(e.detail.value)} >
                      {customer.email} </IonInput>
                  </IonItem>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <IonItem>
                    <IonLabel position="stacked">Phone</IonLabel>
                    <IonInput onIonChange={e => customer.phone=String(e.detail.value)} >
                      {customer.phone} </IonInput>
                  </IonItem>
                </IonCol>
              </IonRow>
              <IonRow>
                <IonCol>
                  <IonItem>
                    <IonLabel position="stacked">Address</IonLabel>
                    <IonInput onIonChange={e=>customer.adress=String(e.detail.value)} >
                      {customer.adress} </IonInput>
                  </IonItem>
                </IonCol>
              </IonRow>





            </IonGrid>

          </IonCard>
        </IonContent>



      </IonContent>
    </IonPage>
  );
};

export default CustomerEdit;
