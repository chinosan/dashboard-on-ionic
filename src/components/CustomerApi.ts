import { Customer } from "./Customer";

export async function searchCustomers(){
    let url = process.env.REACT_APP_API +'/customer'
    let response = await fetch(url,{
        'method': 'Get',
        'headers': {
            'Content-Type': 'application/json'
        }
    });
    return await response.json();
    // if(!localStorage['customers']){
    //     localStorage['customers'] = '[]';
    // }
    // let customers = localStorage['customers'];
    // customers = JSON.parse(customers);
    // return customers;
};

export async function deleteCustomer(id:string){
    let url = process.env.REACT_APP_API +'/customer/'+id;
    await fetch(url,{
        'method': 'DELETE',
        'headers': {
            'Content-Type': 'application/json'
        }
    });
};
export async function saveCustomer(customer:Customer){
    let url = process.env.REACT_APP_API +'/customer'
    let response = await fetch(url,{
        'method': 'POST',
        'body':JSON.stringify(customer),
        'headers': {
            'Content-Type': 'application/json'
        }
    });
    // let customers = await searchCustomers();
    // if(customer.id){
    //     //edit
    //     let indice = customers.findIndex( (c:any) => c.id ==customer.id);
    //     customers.splice(indice, 1);
    // } else{
    //     //add new
    //     customer.id = String(Math.round(Math.random()*100000));
    //     customers.push(customer);
    // }
    
    // localStorage['customers'] = JSON.stringify(customers);   
}
export async function searchCustomerbyId(id:string){
    let url = process.env.REACT_APP_API +'/customer/'+id;
    let response = await fetch(url,{
        'method': 'GET',
        'headers': {
            'Content-Type': 'application/json'
        }
    });
    return await response.json();
}